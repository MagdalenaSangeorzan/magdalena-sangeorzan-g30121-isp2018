package sangeorzan_magdalena_lab8_ex4;


public abstract class Event {
    EventType type;

    Event(EventType type) {
        this.type = type;
    }

    EventType getType() {
        return type;
    }
}