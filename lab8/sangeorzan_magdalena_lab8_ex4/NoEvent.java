package sangeorzan_magdalena_lab8_ex4;

public class NoEvent extends Event {

    NoEvent() {
        super(EventType.NONE);
    }

    @Override
    public String toString() {
        return "NoEvent{}";
    }
}
