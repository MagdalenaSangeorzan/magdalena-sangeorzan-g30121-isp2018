package lab2.magdalena.sangeorzan;

import java.util.Scanner;

public class MAXNRVECT {

    public static void main(String args[]) {
        Scanner scan = new Scanner(System.in);
        System.out.println("n=");
        int n = scan.nextInt(), i=0, max=0;
        int[] vect= new int[n];
        for(i=0;i<n;i++) {
            System.out.println("vect["+i+"]=");
            vect[i] = scan.nextInt();
        }
        for(i=0;i<n;i++) {
            if(vect[i]>max) {
                max=vect[i]; }
        }
        System.out.println("Max is "+max);
    }
}
