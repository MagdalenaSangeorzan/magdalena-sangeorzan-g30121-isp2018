package sangeorzan_magdalena_lab3_ex1;

public class TestAuthor {

    public static void main(String[] args) {

        Author a1 = new Author("anto", "antoschneider", 'm');

        System.out.println(a1.toString());//pentru toString -return
        //   a1.tooString();

        String a = a1.getName();
        System.out.println("getName: " + a);

        String b = a1.getEmail();
        System.out.println("getEmail: " + b);

        char c = a1.getGender();
        System.out.println("getGender: " + c);

        a1.setEmail("popescu.popoviciu1@yahoo.com");

        String d = a1.getEmail();
        System.out.println("getEmail after using setEmail: " + d + "\n");


    }

}
