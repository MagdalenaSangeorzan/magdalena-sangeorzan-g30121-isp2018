package sangeorzan_magdalena_lab3_ex1;

public class Author {

    private String name;
    private String email;
    private char gender;

    public Author(){}

    public Author(String name, String email, char gender) {

        this.name = name;
        this.email = email;
        this.gender = gender;
        // if (g == 'f' || g == 'm') {
        // this.gender = g;
        // } else {
        // System.out.println("Eroare!");
        // }
        // System.out.println("Name:" + " " + n + " " + " " + "Email:" + E + " " + "Gender:" + g);

    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;

    }

    public void setEmail(String email) {
        this.email = email;
    }

    public char getGender() {
        return gender;
    }

    public String toString() {
        return "Author - " + name + " (" + gender + ") at " + email;
    }
   /* public void tooString() {
        System.out.println(this.name + " (" + this.gender + ") at " + this.email);
    }*/

}
