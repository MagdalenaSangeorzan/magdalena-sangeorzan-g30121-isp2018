package sangeorzan_magdalena_lab3_ex3;

public class MyPoint {

    int x=0;
    int y=0;

    public  MyPoint(){
        this.x=0;
        this.y=0;
    }
    public MyPoint(int p1,int p2){
        this.x=p1;
        this.y=p2;
    }
    public void setX(int x){
        this.x=x;
    }
    public void setY(int y){
        this.y=y;
    }
    public int getX(){
        return x;

    }
    public int getY(){
        return y;
    }
    public void setXY(int x,int y){
        this.x=x;
        this.y=y;
    }
    public String tooString(){
        return("x(" + this.x + ","+this.y + ")");
    }
    public double distance(int x, int y) {
        double d;
        d = Math.sqrt(Math.pow(x - this.x, 2) + Math.pow(y - this.y, 2));
        return d;
    }

    public double distance(MyPoint a) {
        double d;
        d = Math.sqrt(Math.pow(a.x - this.x, 2) + Math.pow(a.y - this.y, 2));
        return d;
    }

}
