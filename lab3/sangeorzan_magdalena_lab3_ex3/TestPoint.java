package sangeorzan_magdalena_lab3_ex3;

public class TestPoint {

    public static void main(String[] args){

        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(10, 2);
        p1.setX(4);
        p1.setY(8);
        System.out.println(p1.getX() + " " + p1.getY());
        p2.setXY(1, 4);
        System.out.println(p2.getX() + " " + p2.getY());
        System.out.println("distance1: " + p1.distance(8, 13));
        System.out.println("distance2: " + p1.distance(p2));

    }
}
