package sangeorzan_magdalena_lab12_ex3;


public class ElectricVehicle extends Vehicle {

    public ElectricVehicle (String type, int length) {
        super (type, length);
    }

    public String start () {
        return "electric engine started";
    }
}