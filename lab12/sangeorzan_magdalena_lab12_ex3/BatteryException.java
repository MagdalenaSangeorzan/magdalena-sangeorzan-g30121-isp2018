package sangeorzan_magdalena_lab12_ex3;

public class BatteryException extends Exception {

    BatteryException(String msg){
        super(msg);
    }

}
