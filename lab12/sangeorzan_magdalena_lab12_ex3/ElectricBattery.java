package sangeorzan_magdalena_lab12_ex3;

public class ElectricBattery extends Exception {

    /**
     * Percentage load.
     */
    private int charge = 0;

    public void charge() throws BatteryException {
        if (charge == 100) {
            throw new BatteryException("Error");
        } else
            charge++;


    }
}
