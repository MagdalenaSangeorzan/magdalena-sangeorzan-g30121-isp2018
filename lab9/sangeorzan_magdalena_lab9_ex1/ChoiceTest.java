package sangeorzan_magdalena_lab9_ex1;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

class ChoiceTest extends JFrame implements ItemListener {
    private JLabel label;
    private JComboBox culori;

    public ChoiceTest(String titlu) {
        super(titlu);
        initializare();
        setVisible(true);
    }

    public void initializare() {
        setLayout(new GridLayout(4, 1));

        label = new JLabel("Choose a color");
        label.setOpaque(true);
        label.setBackground(Color.red);

        culori = new JComboBox();
        culori.addItem("Red");
        culori.addItem("Green");
        culori.addItem("Blue");
        culori.setSelectedIndex(0);

        add(label);
        add(culori);
        pack();
        setSize(200, 100);

        culori.addItemListener(this);
    }

    //metoda interfetei ItemListener
    public void itemStateChanged(ItemEvent e) {
        switch (culori.getSelectedIndex()) {
            case 0:
                label.setBackground(Color.red);
                break;
            case 1:
                label.setBackground(Color.green);
                break;
            case 2:
                label.setBackground(Color.blue);
        }
    }

    public static void main(String[] args) {
        new ChoiceTest("Color");
    }
}
