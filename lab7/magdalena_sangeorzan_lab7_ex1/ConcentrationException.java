package magdalena_sangeorzan_lab7_ex1;

public class ConcentrationException extends Exception {
    int c;

    public ConcentrationException(int c,String msg) {
        super(msg);//returneaza un mesaj
        this.c = c;
    }

    int getConc(){
        return c;
    }
}
