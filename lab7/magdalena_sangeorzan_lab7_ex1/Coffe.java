package magdalena_sangeorzan_lab7_ex1;

public class Coffe {
    private int temp;
    private int conc;

    Coffe(int t, int c) {
        this.temp = t;
        this.conc = c;
    }

    int getTemp() {
        return temp;
    }

    int getConc() {
        return conc;
    }

    public String toString() {
        return "[cofee temperature=" + this.temp + ":concentration=" + this.conc + "]";
    }

}
