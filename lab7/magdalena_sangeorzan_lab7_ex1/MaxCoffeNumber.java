package magdalena_sangeorzan_lab7_ex1;

public class MaxCoffeNumber extends Exception {

    public MaxCoffeNumber(String msg){
        super(msg);
    }

}
