package magdalena_sangeorzan_lab7_ex1;
public class CoffeeMaker {

    private static int coffeNumber;

    Coffe makeCoffee () throws MaxCoffeNumber {

        System.out.println("Make a coffe");
        int t = (int) (Math.random() * 100);
        int c = (int) (Math.random() * 100);
        Coffe cofee = new Coffe(t, c);

        coffeNumber++;
        if(coffeNumber>14) throw new MaxCoffeNumber("Can't create more coffes");

        return cofee;

    }

}
