package Sangeorzan_Magdalena_lab11_ex1;

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class TemperatureTextView extends JPanel implements Observer {

    JTextField jtfTemp;
    JLabel jtlTemp;

    TemperatureTextView(){
        this.setLayout(new FlowLayout());
        jtfTemp = new JTextField(20);
        jtlTemp = new JLabel("Temperature");
        add(jtlTemp);add(jtfTemp);
    }

    public void update(Observable o, Object arg) {
        String s = ""+((TemperatureSensor)o).getTemperature();
        jtfTemp.setText(s);
    }
}
