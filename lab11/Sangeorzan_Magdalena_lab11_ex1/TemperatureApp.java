package Sangeorzan_Magdalena_lab11_ex1;

import javax.swing.*;
import java.awt.*;

public class TemperatureApp  extends JFrame {
    TemperatureApp(TemperatureTextView tview){
        setLayout(new BorderLayout());
        add(tview,BorderLayout.NORTH);
        pack();
        setVisible(true);
    }

    public static void main(String[] args) {
        TemperatureSensor t = new TemperatureSensor();
        t.start();

        TemperatureTextView tview = new TemperatureTextView();
        TemperatureController tcontroler = new TemperatureController(t,tview);

        new TemperatureApp(tview);
    }
}
