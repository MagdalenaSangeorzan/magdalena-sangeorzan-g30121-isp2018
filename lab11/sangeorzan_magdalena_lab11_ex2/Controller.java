package sangeorzan_magdalena_lab11_ex2;

public class Controller {
    Products products;
    View view;

    public  Controller(Products products,View view){
        products.addObserver(view);
        this.products=products;
        this.view=view;
    }
}
