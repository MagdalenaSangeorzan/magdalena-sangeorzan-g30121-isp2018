package magdalena_sangeorzan_lab6_ex1;

public class BankAccount {

    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.balance = balance;
        this.owner = owner;
    }

    public void withdraw(double amount) {
        this.balance = this.balance - amount;
    }

    public void deposit(double amount) {
        this.balance = this.balance + amount;
    }

    @Override

    public boolean equals(Object obj) {
        if (obj instanceof BankAccount) {
            BankAccount b = (BankAccount) obj;
            return balance == b.balance;
        }
        return false;
    }

    //hashCode
    public boolean equals1(Object o) {
        if (o == null || !(o instanceof BankAccount))
            return false;
        BankAccount b = (BankAccount) o;
        return b.balance == balance && b.owner.equals(owner);
    }

    public int hashCode() {
        return new Double(balance).intValue();

    }

    public String toString() {
        return owner + ":" + balance;
    }


    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("David", 12);
        BankAccount b2 = new BankAccount("Anna", 12);
        //pentru numere
        if (b1.equals(b2))
            System.out.println(b1 + "    " + "and " + "  " + b2 + "   " + "are equals");
        else
            System.out.println(b1 + "    " + "and " + "  " + b2 + "   " + "are not  equals");
        //pt caractere

        if (b1.owner.equals(b2.owner))
            System.out.println(b1 + "  " + "and" + "  " + b2 + "  " + "have the same owner");
        else
            System.out.println(b1 + "  " + "and" + "  " + b2 + "  " + "have different owner");

    }
}
