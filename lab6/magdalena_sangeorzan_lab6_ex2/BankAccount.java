package magdalena_sangeorzan_lab6_ex2;


    public class BankAccount {
        public String owner;
        public double balance;

        public BankAccount(String owner, double balance) {
            this.balance = balance;
            this.owner = owner;
        }

        public void withdraw(double amount) {
            this.balance = this.balance - amount;

        }

        public void deposit(double amount) {
            this.balance = this.balance - amount;
        }

        public boolean equals(Object obj) {
            if (obj instanceof BankAccount) {
                BankAccount ba = (BankAccount) obj;
                return balance == ba.balance && ba.owner.equals(owner);
            }
            return false;
        }

        public int hasCode() {
            return (int) balance + owner.hashCode();
        }
    }
