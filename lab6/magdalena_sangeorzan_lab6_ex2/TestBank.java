package magdalena_sangeorzan_lab6_ex2;

public class TestBank{
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Dorel", 100);
        bank.addAccount("Ioan", 30);
        bank.addAccount("Andrei", 100);
        bank.addAccount("Maria", 70);
        bank.addAccount("Anto", 810);
        bank.addAccount("Magdalena", 1000);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(10, 9000);
        System.out.println("Get Account by owner name");
        bank.getAccount("Dorel");
        System.out.println("Get All Account order by name");
        bank.getAllAccounts();
    }
}
