package magdalena_sangeorzan_lab4_ex4;

import sangeorzan_magdalena_lab3_ex1;


public class Book extends Author {

    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {

        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public String toString() {
      return "this book" +this.name+ "was written by" + this.authors.length + " authors";
     }

   // public void tooString() {
      //  System.out.println("this book " + this.name + " was written by " + this.authors.length + " different authors");
   // }


    public void printAuthors() {
        for (int i = 0; i < authors.length; i++) {
            System.out.print(this.authors[i].toString() + "\n ");
        }
    }


}



