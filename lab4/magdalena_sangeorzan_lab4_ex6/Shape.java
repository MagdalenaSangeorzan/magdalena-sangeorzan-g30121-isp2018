package magdalena_sangeorzan_lab4_ex6;

public class Shape {

    String color = "red";
    boolean filled = true;

    public Shape() {
        this.color = "green";
        this.filled = true;
    }

    public Shape(String color,boolean filled){
        this.color=color;
        this.filled=filled;
    }
    public String getColor(){
        return color;

    }
    public void setColor(String color){
        this.color=color;
    }
    public boolean isFilled(){
        return filled;
    }
    public void setFilled(boolean filled){
        this.filled=filled;
    }

    public String toString(){
        String s;
        if(this.filled==true)
           s="filled";
           else
               s="not filled";
           return "Shape {" +"color" +  "  " + color + " " + "is"+ " " + s;

    }
}
