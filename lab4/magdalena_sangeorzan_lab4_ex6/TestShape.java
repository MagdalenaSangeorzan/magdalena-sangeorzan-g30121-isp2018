package magdalena_sangeorzan_lab4_ex6;

public class TestShape {

        public static void main(String[] args) {
            Shape s = new Shape("green",true);
            System.out.println(s.toString());

            System.out.println("\n");

            Circle c = new Circle(4,"red",true);
            System.out.println(c.toString());
            System.out.println("Area = "+c.getAria());
            System.out.println("Perimeter = "+c.getPerimetru());
            System.out.println("Is filled = "+c.isFilled());

            System.out.println("\n");

            Rectangle r = new Rectangle(2,3,"black",false);
            System.out.println(r.toString());
            System.out.println("Width = "+r.getWidth());
            r.setWidth(5);
            r.setLength(12);
            System.out.println("New width = "+r.getWidth());
            System.out.println("new length = " +r.getLength());
            System.out.println("Area = "+r.getAria());
            System.out.println("Is filled = "+r.isFilled());

            System.out.println("\n");

            Square sq = new Square(7,"blue",false);
            sq.setSide(3);
            System.out.println(sq.toString());
            System.out.println("Is filled = "+sq.isFilled());
            System.out.println("Perimeter = "+sq.getPerimetru());
            System.out.println("Aria =" + sq.getAria());


        }
}
