package magdalena_sangeorzan_lab4_ex5;

import sangeorzan_magdalena_lab3_ex2;

public class Cylinder extends Circle {

    double height = 1.0;

   public Cylinder() {}


    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return height * Math.PI * super.getRadius() * super.getRadius();
    }

    public double getAria() {
        return 2 * 3.14 * super.getRadius() * this.height + 2 * super.getAria();
    }

}
