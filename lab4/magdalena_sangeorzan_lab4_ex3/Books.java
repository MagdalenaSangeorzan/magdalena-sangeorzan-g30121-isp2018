package magdalena_sangeorzan_lab4_ex3;

import sangeorzan_magdalena_lab3_ex1;

public class Books extends Author{

    private String name;
    private Author author;
    private double price;
    private int qtyInStock;

    public Books(String name, Author author, double price){
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Books(String name, Author author, double price, int qtyInStock) {

        this.name = name;
        this.author = author;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

   public String toString() {
       return "name-book" + name + "by"+" " + author.toString();
   }

  /*  public void tooString() {
        System.out.print("book-name " + this.name + " by :");
        author.toString();
    }
*/

}
