package magdalena_sangeorzan_lab5_ex1;

abstract class Shape {
    protected String color;
    protected boolean filled;

    public Shape() {}

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    abstract double getAria();//metode abstracte =fara implementare in  clasa abstracta
                              //clasa mostenita trebuie sa ofere implementare pt toate metodele abstracte

    abstract double getPerimetru();


    public String toString() {
        String s;
        if (this.filled == true)
            s = "filled";
        else
            s = "not filled";
        return "Shape {" + "color" + "  " + color + " " + "is" + " " + s;
    }

}
