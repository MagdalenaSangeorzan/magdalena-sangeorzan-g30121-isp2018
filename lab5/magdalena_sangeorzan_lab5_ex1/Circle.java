package magdalena_sangeorzan_lab5_ex1;

public class Circle extends Shape {
    protected double radius;

    public Circle() {}

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color, boolean filled) {
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getAria() {
        return 3.14 * this.radius * this.radius;
    }

   public double getPerimetru() {
        return 2 * 3.14 * this.radius;
    }

    public String toString(){
        return  " A circle with radius:" +this.radius + "is a subclass of"+  " " + super.toString();
    }

}
