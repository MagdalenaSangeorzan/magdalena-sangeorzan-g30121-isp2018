package magdalena_sangeorzan_lab5_ex1;

public class Square extends Rectangle {
    public Square() {
    }

    public Square(double side) {
        super(side, side);
        side = side;
    }

    public Square(double side, String color, boolean filled) {
        super(side, side, color, filled);

    }

    public double getSide() {
        return super.getWidth();
    }

    public void setSide(double side) {
        side = super.getWidth();
        side = super.getLength();
        side = side;
    }

   /* public void getWidth(double side) {
        super.getWidth();
    }

    public void getLength(double side) {
        super.getLength();
    }*/

    public double getAria() {
        return this.getSide() * this.getSide();
    }

    public double getPerimetru() {
        return 4 * this.getSide();
    }

    public String toString() {
        return "A Square with side=" + this.getSide() + " ,which is a subclass of" + super.toString();
    }
}
