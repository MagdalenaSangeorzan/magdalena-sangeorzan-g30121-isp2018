package magdalena_sangeorzan_lab5_ex2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private String fileName;
    private String rotate;


    public ProxyImage(String fileName){
        this.fileName = fileName;
        //this.rotate ="";
    }

    public ProxyImage(String fileName,String rotate){
        this.fileName = fileName;
        this.rotate=rotate;
    }


    public void RotatedImage()
    {
        System.out.println("Rotated Image "+fileName);
    }


    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        if (this.rotate == null)
        {
            realImage.display();
        }
        else  realImage.RotatedImage();
    }
}

