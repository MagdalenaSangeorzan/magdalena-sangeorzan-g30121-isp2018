package magdalena_sangeorzan_lab5_ex2;

public class TestImage {
    public static void main(String[] args) {
        RealImage RealImg = new RealImage("Img.jpg");
        ProxyImage ProxyImg = new ProxyImage("Img1.jpg", "left");
        ProxyImage ProxyImg2 = new ProxyImage("Img2.jpg");
        ProxyImage ProxyImg1 = new ProxyImage("Img3.jpg", "right");


        RealImg.display();
        ProxyImg.display();
        ProxyImg2.display();
        ProxyImg1.display();
    }
}
