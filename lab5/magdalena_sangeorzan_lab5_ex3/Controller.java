package magdalena_sangeorzan_lab5_ex3;

public class Controller { //va citi și afișa valorile temperaturii și luminii cu o perioadă de 1 secundă pe o durată de 20 de secunde;

    public void control() throws InterruptedException {

         TemperatureSensor s1 = new TemperatureSensor();
         LightSensor s2 = new LightSensor();

        int sec = 1;
        while (sec <= 20) {
            System.out.println("temp:" + s1.readValue());
            System.out.println("light:" + s2.readValue());
            System.out.println("sec: " + sec);
            sec++;
            Thread.sleep(1000);//pt a putea intelege ochiul uman cum trec 20 sec:delay
        }
    }
}
